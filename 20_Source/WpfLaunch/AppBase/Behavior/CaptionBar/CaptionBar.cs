﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace WpfLaunch.Behavior.CaptionBar
{
    public class BehaveFixedsizeWindow : Behavior<Grid>
    {
        public BehaveFixedsizeWindow()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += mouseLeftButtonDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= mouseLeftButtonDown;
        }

        private void mouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                if (e.ButtonState == MouseButtonState.Pressed)
                {
                    window.DragMove();
                }
            }
        }
    }

    public class BehaveResizableWindow : Behavior<Grid>
    {
        public BehaveResizableWindow()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += mouseLeftButtonDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= mouseLeftButtonDown;
        }

        private void mouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
                {
                    switch (window.WindowState)
                    {
                        case WindowState.Normal: window.WindowState = WindowState.Maximized; break;
                        case WindowState.Maximized: window.WindowState = WindowState.Normal; break;
                        case WindowState.Minimized: break;
                    }
                }
                else
                {
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        window.DragMove();
                    }
                }
            }
        }
    }
}
