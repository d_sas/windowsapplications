﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Interop;

namespace WpfLaunch.Behavior.DisableKey
{
    public class DisableAltF4 : Behavior<Window>
    {
        private int WM_SYSKEYDOWN = 0x0104;
        private int VK_F4 = 0x73;

        protected override void OnAttached()
        {
            this.AssociatedObject.SourceInitialized += (sender, e) =>
            {
                var handle = new WindowInteropHelper(this.AssociatedObject).Handle;
                var hwndSource = HwndSource.FromHwnd(handle);
                hwndSource.AddHook(this.wndProc);
            };
        }

        private IntPtr wndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_SYSKEYDOWN & wParam.ToInt32() == VK_F4)
            {
                handled = true;
            }

            return IntPtr.Zero;
        }
    }
}
