﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace WpfLaunch.Behavior.WindowSize
{
    public class MinimizedWindow : Behavior<Button>
    {
        public MinimizedWindow()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Click += minimizedWindowButtonClick;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.Click -= minimizedWindowButtonClick;
        }

        private void minimizedWindowButtonClick(object sender, RoutedEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                switch (window.WindowState)
                {
                    case WindowState.Normal: window.WindowState = WindowState.Minimized; break;
                    case WindowState.Maximized: window.WindowState = WindowState.Minimized; break;
                    case WindowState.Minimized: break;
                }
            }
        }
    }

    public class ResizeWindow : Behavior<Button>
    {
        public ResizeWindow()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Click += resizeWindowButtonClick;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.Click -= resizeWindowButtonClick;
        }

        private void resizeWindowButtonClick(object sender, RoutedEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                switch (window.WindowState)
                {
                    case WindowState.Normal: window.WindowState = WindowState.Maximized; break;
                    case WindowState.Maximized: window.WindowState = WindowState.Normal; break;
                    case WindowState.Minimized: break;
                }
            }
        }
    }
}
