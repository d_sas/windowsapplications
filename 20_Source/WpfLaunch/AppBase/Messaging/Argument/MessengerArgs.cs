﻿using System;

namespace WpfLaunch.Messasing.Argument
{
    public class MessengerActionEventArgs : EventArgs
    {
        public Action Callback { get; private set; }

        public MessengerActionEventArgs(Action callback)
        {
            Callback = callback;
        }
    }

    public class MessengerActionEventArgs<TParam> : EventArgs
    {
        public TParam Message { get; private set; }
        public Action Callback { get; private set; }

        public MessengerActionEventArgs(TParam message, Action callback)
        {
            Message = message;
            Callback = callback;
        }
    }

    public class MessengerFuncEventArgs<TResult> : EventArgs
    {
        public Action<TResult> Callback { get; private set; }

        public MessengerFuncEventArgs(Action<TResult> callback)
        {
            Callback = callback;
        }
    }

    public class MessengerFuncEventArgs<TParam, TResult> : EventArgs
    {
        public TParam Message { get; private set; }
        public Action<TResult> Callback { get; private set; }

        public MessengerFuncEventArgs(TParam message, Action<TResult> callback)
        {
            Message = message;
            Callback = callback;
        }
    }
}
