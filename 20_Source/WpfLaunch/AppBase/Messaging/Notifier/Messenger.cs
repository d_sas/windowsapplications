﻿using System;
using WpfLaunch.Messasing.Argument;

namespace WpfLaunch.Messasing.Notifier
{
    public class Messenger
    {
        public event EventHandler Raised;

        public void Notify()
        {
            Raised?.Invoke(null, null);
        }
    }

    public class Messenger<TParam>
    {
        public event EventHandler<MessengerActionEventArgs<TParam>> Raised;

        public void Notify(TParam message, Action callback)
        {
            Raised?.Invoke(this, new MessengerActionEventArgs<TParam>(message, callback));
        }
    }

    public class Messenger<TParam, TResult>
    {
        public event EventHandler<MessengerFuncEventArgs<TParam, TResult>> Raised;

        public void Notify(TParam message, Action<TResult> callback)
        {
            Raised?.Invoke(this, new MessengerFuncEventArgs<TParam, TResult>(message, callback));
        }
    }
}
