﻿using System.Windows.Interactivity;

namespace WpfLaunch.Messasing.Trigger
{
    public class MessageTrigger : EventTrigger
    {
        protected override string GetEventName()
        {
            return "Raised";
        }
    }
}
