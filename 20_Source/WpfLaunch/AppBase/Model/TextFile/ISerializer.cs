﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfLaunch.Model.TextFile
{
    public interface ISerializer<T>
    {
        SerializeResult<T> Save(T obj);
        SerializeResult<T> Load();
    }

    public class SerializeResult<T>
    {
        public FileAccessResult Result { get; private set; }
        public T Obj { get; private set; }

        public SerializeResult(FileAccessResult result, T obj)
        {
            Result = result;
            Obj = obj;
        }
    }

    public enum FileAccessResult
    {
        Default,
        Ok,
        UnauthorizeFile,
        FileNotFound,
        PathTooLong,
        OthersUse,
        InvalidFormat,
    }
}
