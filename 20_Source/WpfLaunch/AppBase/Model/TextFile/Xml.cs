﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WpfLaunch.Model.TextFile
{
    public class Xml<T> : ISerializer<T>
    {
        public string ExceptionMessage { get; private set; }

        private string filePath = null;

        public Xml(string filePath)
        {
            this.filePath = filePath;
        }

        public SerializeResult<T> Save(T obj)
        {
            var fileAccessResult = FileAccessResult.Ok;

            try
            {
                var serializer = new XmlSerializer(typeof(T));
                using (var stream = new StreamWriter(this.filePath, false, new UTF8Encoding(false)))
                {
                    serializer.Serialize(stream, obj);
                }
            }
            catch (UnauthorizedAccessException ex) { ExceptionMessage = ex.Message; fileAccessResult = FileAccessResult.UnauthorizeFile; }
            catch (DirectoryNotFoundException ex) { ExceptionMessage = ex.Message; fileAccessResult = FileAccessResult.FileNotFound; }
            catch (PathTooLongException ex) { ExceptionMessage = ex.Message; fileAccessResult = FileAccessResult.PathTooLong; }
            catch (IOException ex) { ExceptionMessage = ex.Message; fileAccessResult = FileAccessResult.OthersUse; }

            var result = new SerializeResult<T>(fileAccessResult, default(T));

            return result;
        }

        public SerializeResult<T> Load()
        {
            T obj = default(T);
            FileAccessResult accessResult = FileAccessResult.Ok;
            ExceptionMessage = null;

            try
            {
                using (StreamReader stream = new StreamReader(this.filePath))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    obj = (T)serializer.Deserialize(stream);
                }
            }
            catch (UnauthorizedAccessException ex) { ExceptionMessage = ex.Message; accessResult = FileAccessResult.UnauthorizeFile; }
            catch (FileNotFoundException ex) { ExceptionMessage = ex.Message; accessResult = FileAccessResult.FileNotFound; }
            catch (DirectoryNotFoundException ex) { ExceptionMessage = ex.Message; accessResult = FileAccessResult.FileNotFound; }
            catch (IOException ex) { ExceptionMessage = ex.Message; accessResult = FileAccessResult.OthersUse; }
            catch (InvalidOperationException ex) { ExceptionMessage = ex.Message; accessResult = FileAccessResult.InvalidFormat; }

            var result = new SerializeResult<T>(accessResult, obj);

            return result;
        }
    }
}
