﻿using System.Windows;
using System.Windows.Interactivity;
using WpfLaunch.Messasing.Argument;
using WpfLaunch.View.Alert.Parameter;

namespace WpfLaunch.View.Alert.Action
{
    public class AlertAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessengerFuncEventArgs<AlertParameter, bool>;
            if (args != null)
            {
                var dialog = new AlertView(args.Message);
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                dialog.Owner = Window.GetWindow(AssociatedObject);
                var result = (bool)dialog.ShowDialog();
                args.Callback?.Invoke(result);
            }
        }
    }
}
