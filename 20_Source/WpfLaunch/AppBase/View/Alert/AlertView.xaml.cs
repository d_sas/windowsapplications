﻿using System.Windows;
using WpfLaunch.View.Alert.Parameter;
using WpfLaunch.ViewModel.Alert;

namespace WpfLaunch.View.Alert
{
    /// <summary>
    /// AlertView.xaml の相互作用ロジック
    /// </summary>
    public partial class AlertView : Window
    {
        public AlertView(AlertParameter parameter)
        {
            this.DataContextChanged += (s, e) =>
            {
                var vm = e.NewValue as AlertViewModel;
                vm.SetParameter(parameter);
            };

            InitializeComponent();
        }
    }
}
