﻿namespace WpfLaunch.View.Alert.Parameter
{
    public enum MessageType
    {
        Ok,
        OkCancel,
        YesNo,
    }

    public enum IconType
    {
        None,
        Information,
        Warning,
        Error,
    }

    public class AlertParameter
    {
        public MessageType MessageType { get; private set; }
        public IconType IconType { get; private set; }
        public string Title { get; private set; }
        public string Message { get; private set; }

        public AlertParameter(MessageType type, IconType iconType, string title, string message)
        {
            MessageType = type;
            IconType = iconType;
            Title = title;
            Message = message;
        }

        public AlertParameter(string title, string message)
        {
            MessageType = MessageType.Ok;
            IconType = IconType.Information;
            Title = title;
            Message = message;
        }
    }
}
