﻿using System.Windows;
using System.Windows.Interactivity;

namespace WpfLaunch.View.Common.Action
{
    public class CloseWindowAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var window = Window.GetWindow(AssociatedObject);
            window.Close();
        }
    }
}
