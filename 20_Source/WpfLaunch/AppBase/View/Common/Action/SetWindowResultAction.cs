﻿using System.Windows;
using System.Windows.Interactivity;
using WpfLaunch.Messasing.Argument;

namespace WpfLaunch.View.Common.Action
{
    public class SetWindowResultAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessengerActionEventArgs<bool>;
            if (args != null)
            {
                var result = args.Message;
                var window = Window.GetWindow(AssociatedObject);
                window.DialogResult = result;
            }
        }
    }
}
