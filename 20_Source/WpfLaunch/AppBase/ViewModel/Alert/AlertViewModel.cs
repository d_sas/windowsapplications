﻿using System;
using WpfLaunch.Messasing.Notifier;
using WpfLaunch.View.Alert.Parameter;

namespace WpfLaunch.ViewModel.Alert
{
    public class AlertViewModel : PropertyChangeNotifier
    {
        public Messasing.Notifier.Messenger CloseWindowMessenger { get { return this.closeWindowMessenger ?? (this.closeWindowMessenger = new Messasing.Notifier.Messenger()); } }
        private Messasing.Notifier.Messenger closeWindowMessenger;

        public Messenger<bool> SetWindowResultMessenger { get { return this.setWindowResultMessenger ?? (this.setWindowResultMessenger = new Messenger<bool>()); } }
        private Messenger<bool> setWindowResultMessenger;

        public DelegateCommand CloseWindowCommand { get { return this.closeWindowCommand ?? (this.closeWindowCommand = new DelegateCommand(closeWindow)); } }
        private DelegateCommand closeWindowCommand;

        public DelegateCommand SelectPotiveCommand { get { return this.selectPotiveCommand ?? (this.selectPotiveCommand = new DelegateCommand(selectPotive)); } }
        private DelegateCommand selectPotiveCommand;

        public DelegateCommand SelectNegativeCommand { get { return this.selectNegativeCommand ?? (this.selectNegativeCommand = new DelegateCommand(selectNegative)); } }
        private DelegateCommand selectNegativeCommand;

        public string Title { get { return this.title; } set { this.title = value; NotifyPropertyChanged(nameof(Title)); } }
        private string title;

        public IconType Icon { get { return this.icon; } set { this.icon = value; NotifyPropertyChanged(nameof(Icon)); } }
        private IconType icon;

        public string Message { get { return this.message; } set { this.message = value; NotifyPropertyChanged(nameof(Message)); } }
        private string message;

        public string PositiveContent { get { return this.positiveContent; } set { this.positiveContent = value; NotifyPropertyChanged(nameof(PositiveContent)); } }
        private string positiveContent;

        public string NegativeContent { get { return this.negativeContent; } set { this.negativeContent = value; NotifyPropertyChanged(nameof(NegativeContent)); } }
        private string negativeContent;

        public System.Windows.Visibility NegativeEnable { get; set; }

        public void SetParameter(AlertParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException(nameof(parameter)); }

            switch (parameter.MessageType)
            {
                case MessageType.Ok: PositiveContent = "OK"; NegativeContent = ""; NegativeEnable = System.Windows.Visibility.Collapsed; break;
                case MessageType.OkCancel: PositiveContent = "OK"; NegativeContent = "Cancel"; break;
                case MessageType.YesNo: PositiveContent = "Yes"; NegativeContent = "No"; break;
            }

            Icon = parameter.IconType;
            switch (Icon)
            {
                case IconType.Information: System.Media.SystemSounds.Asterisk.Play(); break;
                case IconType.Warning: System.Media.SystemSounds.Beep.Play(); break;
                case IconType.Error: System.Media.SystemSounds.Hand.Play(); break;
            }

            Title = parameter.Title;

            Message = parameter.Message;
        }

        private void closeWindow()
        {
            CloseWindowMessenger.Notify();
        }

        private void selectPotive()
        {
            SetWindowResultMessenger.Notify(true, null);
        }

        private void selectNegative()
        {
            SetWindowResultMessenger.Notify(false, null);
        }
    }
}
