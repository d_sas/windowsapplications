﻿using System;
using System.Windows.Input;

namespace WpfLaunch.ViewModel
{
    public class DelegateCommand : ICommand
    {
        private Action execute;
        private Func<bool> canExecute;

        public DelegateCommand(Action execute)
            : this(execute, () => true)
        {
        }

        public DelegateCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }
            if (canExecute == null) { throw new ArgumentNullException(nameof(canExecute)); }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            this.execute();
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        private Action<T> execute;
        private Func<bool> canExecute;

        public DelegateCommand(Action<T> execute)
            : this(execute, () => true)
        {
        }

        public DelegateCommand(Action<T> execute, Func<bool> canExecute)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }
            if (canExecute == null) { throw new ArgumentNullException(nameof(canExecute)); }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            this.execute((T)parameter);
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
