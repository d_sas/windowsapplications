﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfLaunch.Messasing.Notifier;

namespace WpfLaunch.ViewModel.SampleAppMain
{
    public class MainViewModel
    {
        public Messenger CloseWindowMessenger { get { return this.closeWindowMessenger ?? (this.closeWindowMessenger = new Messenger()); } }
        private Messenger closeWindowMessenger;

        public DelegateCommand CloseWindowCommand { get { return this.closeWindowCommand ?? (this.closeWindowCommand = new DelegateCommand(closeWindow)); } }
        private DelegateCommand closeWindowCommand;

        private void closeWindow()
        {
            CloseWindowMessenger.Notify();
        }
    }
}
