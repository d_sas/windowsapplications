﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfLaunch.View.MediaPlayerMain;

namespace WpfLaunch.MediaPlayerMain
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            var view = new MainView();
            view.Top = 100;
            view.Left = 500;
            view.Width = 200;
            view.Height = 500;
            view.Show();
        }
    }
}
