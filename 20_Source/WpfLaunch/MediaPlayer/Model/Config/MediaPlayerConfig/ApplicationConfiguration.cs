﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfLaunch.Model.Config.MediaPlayerConfig
{
    public class Config
    {
        public General General { get; set; }
        public Specific Specific { get; set; }
    }

    public class General
    {
        public WindowState WindowState { get; set; }
    }

    public class WindowState
    {
        public int Top { get; set; }
        public int Left { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }

    public class Specific
    {
        public int Volume { get; set; }
    }
}
