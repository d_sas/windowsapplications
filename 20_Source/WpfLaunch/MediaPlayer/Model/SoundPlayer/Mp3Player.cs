﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfLaunch.Model.SoundPlayer
{
    public class MP3Player
    {
        private enum State
        {
            Default,
            None,
            Play,
            Pause,
        }

        private System.Windows.Media.MediaPlayer player;
        private State state = State.Default;
        private Action callback = null;

        public MP3Player(Action callback)
        {
            this.player = new System.Windows.Media.MediaPlayer();
            this.player.MediaEnded += new EventHandler(terminateNotify);
            this.callback = callback;
            this.state = State.None;
        }

        public void Play(Uri filePath)
        {
            switch (this.state)
            {
            case State.None:
                this.player.Open(filePath);
                this.player.Play();
                this.state = State.Play;
                break;
            case State.Play:
                break;
            case State.Pause:
                this.player.Play();
                this.state = State.Play;
                break;
            }
        }

        public void Stop()
        {
            switch (this.state)
            {
            case State.None:
                break;
            case State.Play:
                this.player.Stop();
                this.player.Close();
                this.state = State.None;
                break;
            case State.Pause:
                this.player.Stop();
                this.player.Close();
                this.state = State.None;
                break;
            }
        }

        public void Pause()
        {
            switch (this.state)
            {
            case State.None:
                break;
            case State.Play:
                this.player.Pause();
                this.state = State.Pause;
                break;
            case State.Pause:
                this.player.Play();
                this.state = State.Play;
                break;
            }
        }

        private void terminateNotify(object sender, EventArgs e)
        {
            Stop();
            callback?.Invoke();
        }
    }
}
