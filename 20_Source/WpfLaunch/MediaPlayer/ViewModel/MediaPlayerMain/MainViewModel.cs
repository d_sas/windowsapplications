﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfLaunch.Messasing.Notifier;
using WpfLaunch.Model.SoundPlayer;
using WpfLaunch.View.Alert.Parameter;

namespace WpfLaunch.ViewModel.MediaPlayerMain
{
    public class MainViewModel : PropertyChangeNotifier
    {
        public Messenger CloseWindowMessenger { get { return this.closeWindowMessenger ?? (this.closeWindowMessenger = new Messenger()); } }
        private Messenger closeWindowMessenger;

        public Messenger<AlertParameter, bool> AlertMessenger { get { return this.alertMessenger ?? (this.alertMessenger = new Messenger<AlertParameter, bool>()); } }
        private Messenger<AlertParameter, bool> alertMessenger;

        public DelegateCommand CloseWindowCommand { get { return this.closeWindowCommand ?? (this.closeWindowCommand = new DelegateCommand(closeWindow)); } }
        private DelegateCommand closeWindowCommand;

        public DelegateCommand PlayCommand { get { return this.playCommand ?? (this.playCommand = new DelegateCommand(play)); } }
        private DelegateCommand playCommand;

        public DelegateCommand StopCommand { get { return this.stopCommand ?? (this.stopCommand = new DelegateCommand(stop)); } }
        private DelegateCommand stopCommand;

        public DelegateCommand PauseCommand { get { return this.pauseCommand ?? (this.pauseCommand = new DelegateCommand(pause)); } }
        private DelegateCommand pauseCommand;

        public string SoundFilePath { get; set; }

        private MP3Player player;

        public MainViewModel()
        {
            player = new MP3Player(notifyTerminate);
        }

        private void closeWindow()
        {
            CloseWindowMessenger.Notify();
        }

        private void play()
        {
            player.Play(new Uri(SoundFilePath));
        }

        private void stop()
        {
            player.Stop();
        }

        private void pause()
        {
            player.Pause();
        }

        private void notifyTerminate()
        {
        }

        public DelegateCommand CreateConfigCommand { get { return this.createConfigCommand ?? (this.createConfigCommand = new DelegateCommand(createConfig)); } }
        private DelegateCommand createConfigCommand;
        private void createConfig()
        {

        }
    }
}
